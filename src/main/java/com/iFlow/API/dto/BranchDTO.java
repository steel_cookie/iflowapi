package com.iFlow.API.dto;

import com.iFlow.API.domain.Branch;
import com.iFlow.API.domain.Service;
import lombok.*;

import java.util.ArrayList;
import java.util.List;

@Data
@Builder
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
public class BranchDTO {

    private long branchId;
    private String branchName;
    private String openingTime;
    private String closingTime;

    private List<ServiceDTO> services;

    public List<Service> getServices(Branch branch) {
        List<Service> domainServices = new ArrayList<>();
        for (ServiceDTO serviceDTO : services) {
            Service service = Service.builder()
                    .serviceId(serviceDTO.getServiceId())
                    .serviceName(serviceDTO.getServiceName())
                    .branch(branch)
                    .build();
            domainServices.add(service);
        }
        return domainServices;
    }

}
