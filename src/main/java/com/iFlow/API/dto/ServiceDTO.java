package com.iFlow.API.dto;

import lombok.*;

@Data
@Builder
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
public class ServiceDTO {

    private long serviceId;
    private String serviceName;

    private BranchDTO branch;

}
