package com.iFlow.API.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

import static javax.persistence.CascadeType.ALL;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name="branch")
public class Branch {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private long branchId;
    private String branchName;
    private String openingTime;
    private String closingTime;

    @OneToMany(cascade=ALL, mappedBy="branch")
    private List<Service> services;

}
