package com.iFlow.API.domain;

import javafx.concurrent.Worker;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;

import static javax.persistence.CascadeType.ALL;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name="service")
public class Service {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private long id;
    private long serviceId;
    private String serviceName;

    @ManyToOne(targetEntity=Branch.class, cascade=ALL)
    @JoinColumn(name="branch_branchId")
    private Branch branch;

}
