package com.iFlow.API.service;

import com.iFlow.API.domain.Branch;
import com.iFlow.API.dto.BranchDTO;
import com.iFlow.API.repository.BranchRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BranchService {

    @Autowired
    BranchRepository branchRepository;

    public List<BranchDTO> saveListBranches(List<BranchDTO> branches) {
        for (BranchDTO branchDTO : branches) {
            Branch branch = Branch.builder()
                    .branchName(branchDTO.getBranchName())
                    .openingTime(branchDTO.getOpeningTime())
                    .closingTime(branchDTO.getClosingTime())
                    .build();
            branch.setServices(branchDTO.getServices(branch));
            branchRepository.save(branch);
        }
        return branches;
    }

}
