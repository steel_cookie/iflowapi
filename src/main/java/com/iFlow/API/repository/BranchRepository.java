package com.iFlow.API.repository;

import com.iFlow.API.domain.Branch;
import lombok.NonNull;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BranchRepository extends JpaRepository<Branch, Long> {

    Branch findOneByBranchId (@NonNull Long branchId);

}
