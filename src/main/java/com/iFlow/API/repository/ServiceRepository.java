package com.iFlow.API.repository;

import com.iFlow.API.domain.Service;
import lombok.NonNull;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ServiceRepository extends JpaRepository<Service, Long> {

    Service findOneByServiceId (@NonNull Long serviceId);

}
