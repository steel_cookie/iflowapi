package com.iFlow.API.controller;

import com.iFlow.API.dto.BranchDTO;
import com.iFlow.API.service.BranchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class BranchController {

    @Autowired
    private BranchService branchService;

    @PutMapping("saveBranches")
    public void saveListBranches(@RequestBody List<BranchDTO> branchesDTO) {
        branchService.saveListBranches(branchesDTO);
    }

    @GetMapping("/hello")
    public String getHello() {
        return "Hello from API";
    }

}
